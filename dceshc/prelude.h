#ifndef __PRELUDE_H__
#define __PRELUDE_H__

#include "mpi.h"
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define MALLOC malloc
#define ERROR(...) fprintf(stderr, "********" __VA_ARGS__); exit(0);
#ifdef NDEBUG
#define PRINT_DEBUG(...) printf(__VA_ARGS__);
#define PRINT_DEBUG_INFO {int rank; MPI_Comm_rank(MPI_COMM_WORLD, &rank); PRINT_DEBUG("%d entering %s\n", rank, __FUNCTION__);}
#else
#define PRINT_DEBUG(...)
#define PRINT_DEBUG_INFO
#endif

#define LOCATION_NONE (-1)

/* An int of the architecture's pointer size */
typedef intptr_t int_t;

struct Env_t; typedef struct Env_t* Env;
struct Stack_t; typedef struct Stack_t* Stack;

#define STACK_SIZE 100

typedef void(*Code)(Env, Stack);
/*****************************************************************************
 * Init
 *****************************************************************************/
static inline void initialise(int argc, char** argv) {
  int provided;
  if(MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided) != MPI_SUCCESS) {
    ERROR("MPI_Init failed!\n");
  }
  if(provided != MPI_THREAD_MULTIPLE) {
    printf("******* The MPI environment could not provide the required level of\n"
           "******* thread support. This may lead to deadlocks.\n"
           "******* Proceeding...\n");
  }
}

/* ----------------------------------------------------------------------------
 * Values
 * ---------------------------------------------------------------------------*/
typedef struct RemotePtr {
  void* ptr;
  int location;
} RemotePtr;

typedef union Value {
  RemotePtr closure;
  int_t integer;
} Value;

static inline Value integer_value(int_t i) {
  Value result;
  result.closure.location = LOCATION_NONE;
  result.integer = i;
  return result;
}

static inline void print_value(Value v);
/* ----------------------------------------------------------------------------
 * Environments
 * ---------------------------------------------------------------------------*/
struct Env_t {
  Value* top;
  Value* bottom;
};

static inline void print_env(Env env) {
  PRINT_DEBUG("Env: ");
  for(Value* v = env->bottom - 1; v >= env->top; --v) {
    print_value(*v); PRINT_DEBUG(", ");
  }
  PRINT_DEBUG("[]\n");
}

static inline size_t size_env(Env env) {
  PRINT_DEBUG_INFO;
  print_env(env);
  return env->bottom - env->top;
}

static inline Value* access_env(Env env, size_t index) {
  PRINT_DEBUG_INFO;
  print_env(env);
  return env->top + index;
}

static inline void push_env(Env env, Value val) {
  PRINT_DEBUG_INFO;
  *env->bottom = val;
  ++env->bottom;
  print_env(env);
}

static inline void pop_env(Env env) {
  PRINT_DEBUG_INFO;
  --env->bottom;
  print_env(env);
}

static inline Env malloc_env(size_t max_size) {
  PRINT_DEBUG_INFO;
  Value* arr = MALLOC(sizeof(Value) * max_size);
  Env res = MALLOC(sizeof(struct Env_t));
  res->top = res->bottom = arr;
  return res;
}

static inline Env copy_env(Env env) {
  PRINT_DEBUG_INFO;
  size_t size = size_env(env);
  Env copy = malloc_env(size);
  for(size_t i = 0; i < size; ++i) {
    push_env(copy, *access_env(env, i));
  }
  print_env(env);
  return copy;
}

/* ----------------------------------------------------------------------------
 * Closures
 * ---------------------------------------------------------------------------*/

typedef struct Closure {
  Code code;
  Env env;
} Closure;

static inline Closure* suspend_closure(Closure c) {
  PRINT_DEBUG_INFO;
  Closure* res = MALLOC(sizeof(Closure));
  *res = c;
  return res;
}

static inline RemotePtr remote_ptr(void* val) {
  PRINT_DEBUG_INFO;
  RemotePtr res;
  MPI_Comm_rank(MPI_COMM_WORLD, &res.location);
  res.ptr = val;
  return res;
}

static inline Value remote_ptr_value(RemotePtr ptr) {
  PRINT_DEBUG_INFO;
  Value res;
  res.closure = ptr;
  return res;
}

static inline void print_value(Value v) {
  if(v.closure.location != LOCATION_NONE) {
    PRINT_DEBUG("cls");
    int my_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    if(v.closure.location == my_rank) {
      PRINT_DEBUG("@%d(local)", v.closure.location);
    } else {
      PRINT_DEBUG("@%d", v.closure.location);
    }
  } else {
    PRINT_DEBUG("%" PRIdPTR, v.integer);
  }
}

/* ----------------------------------------------------------------------------
 * Stacks
 * ---------------------------------------------------------------------------*/
struct Stack_t {
  Value* top;
  Value* bottom;
};

static inline void print_stack(Stack stack) {
  PRINT_DEBUG("Stack: ");
  for(Value* v = stack->bottom - 1; v >= stack->top; --v) {
    print_value(*v); PRINT_DEBUG(", ");
  }
  PRINT_DEBUG("[]\n");
}

static inline Stack malloc_stack() {
  PRINT_DEBUG_INFO;
  Value* arr = MALLOC(sizeof(Value) * STACK_SIZE);
  Stack res = MALLOC(sizeof(struct Stack_t));
  res->top = res->bottom = arr;
  return res;
}

static inline Value pop_stack(Stack stack) {
  PRINT_DEBUG_INFO;
  --stack->bottom;
  if(stack->bottom < stack->top) {
    ERROR("Trying to pop the empty stack");
  }
  print_stack(stack);
  return *stack->bottom;
}

static inline void push_stack(Stack stack, Value val) {
  PRINT_DEBUG_INFO;
  *stack->bottom = val;
  ++stack->bottom;
  print_stack(stack);
}

static inline Stack* suspend_stack(Stack stack) {
  PRINT_DEBUG_INFO;
  Stack* res = MALLOC(sizeof(Stack));
  *res = stack;
  return res;
}

/* ----------------------------------------------------------------------------
 * Communication
 * ---------------------------------------------------------------------------*/
#define REMOTE_MSG_TAG 0
#define RETURN_MSG_TAG 1
#define APPLY_MSG_TAG  2

typedef struct RemoteMessage {
  int code;
  RemotePtr ptr;
} RemoteMessage;

static inline void send_remote(int rank, int code, Stack stack) {
  PRINT_DEBUG_INFO;
  RemoteMessage msg;
  msg.code = code;
  msg.ptr  = remote_ptr(suspend_stack(stack));
  PRINT_DEBUG("Sending remote message to %d\n", rank);
  MPI_Send(&msg, sizeof(RemoteMessage),
           MPI_BYTE, rank, REMOTE_MSG_TAG, MPI_COMM_WORLD);
}

typedef struct ReturnMessage {
  Stack* cont;
  Value value;
} ReturnMessage;

static inline void send_return(RemotePtr p, Value v) {
  PRINT_DEBUG_INFO;
  ReturnMessage msg;
  msg.cont = p.ptr;
  msg.value = v;
  PRINT_DEBUG("Sending return message to %d\n", p.location);
  MPI_Send(&msg, sizeof(ReturnMessage),
           MPI_BYTE, p.location, RETURN_MSG_TAG, MPI_COMM_WORLD);
}

static inline void receive_return(ReturnMessage msg) {
  PRINT_DEBUG_INFO;
  Stack stack = *msg.cont;
  Closure* cl = pop_stack(stack).closure.ptr;
  push_stack(stack, msg.value);
  cl->code(cl->env, stack);
}

typedef struct ApplyMessage {
  Closure* ptr;
  Value value;
  RemotePtr cont;
} ApplyMessage;

static inline void send_apply(RemotePtr dest, Value v, Stack cont) {
  PRINT_DEBUG_INFO;
  ApplyMessage msg;
  msg.ptr = dest.ptr;
  msg.value = v;
  msg.cont = remote_ptr(suspend_stack(cont));
  PRINT_DEBUG("Sending apply message to %d\n", dest.location);
  MPI_Send(&msg, sizeof(ApplyMessage),
           MPI_BYTE, dest.location, APPLY_MSG_TAG, MPI_COMM_WORLD);
}

static inline void receive_apply(ApplyMessage msg) {
  PRINT_DEBUG_INFO;
  Stack stack = malloc_stack();
  push_stack(stack, remote_ptr_value(msg.cont));
  Closure cl = *msg.ptr;
  Env clEnv = copy_env(cl.env);
  push_env(clEnv, msg.value);
  cl.code(clEnv, stack);
}

void receive_remote(int, Env, Stack);

static inline void main_loop() {
  while(1) {
    PRINT_DEBUG_INFO;
    MPI_Status status;
    MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
    int count;
    MPI_Get_count(&status, MPI_BYTE, &count);
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    PRINT_DEBUG("Node %d received tag: %d\n", rank, status.MPI_TAG);

    switch (status.MPI_TAG) {
      case REMOTE_MSG_TAG: {
        PRINT_DEBUG("received remote msg\n");
        RemoteMessage msg;
        MPI_Recv(&msg, count, MPI_BYTE, MPI_ANY_SOURCE, MPI_ANY_TAG,
                 MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        Env env = malloc_env(100);
        Stack stack = malloc_stack();
        push_stack(stack, remote_ptr_value(msg.ptr));
        receive_remote(msg.code, env, stack);
        break;
      }
      case RETURN_MSG_TAG: {
        PRINT_DEBUG("received return msg\n");
        ReturnMessage msg;
        MPI_Recv(&msg, count, MPI_BYTE, MPI_ANY_SOURCE, MPI_ANY_TAG,
                 MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        receive_return(msg);
        break;
      }
      case APPLY_MSG_TAG: {
        PRINT_DEBUG("received apply msg\n");
        ApplyMessage msg;
        MPI_Recv(&msg, count, MPI_BYTE, MPI_ANY_SOURCE, MPI_ANY_TAG,
                 MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        receive_apply(msg);
        break;
      }
      default: break;
    }
  }
}
/* ----------------------------------------------------------------------------
 * Instructions
 * ---------------------------------------------------------------------------*/
static inline Value closure(Code code, Env env) {
  PRINT_DEBUG_INFO;
  Closure* clos = MALLOC(sizeof(Closure));
  clos->code = code;
  clos->env = env;
  Value v;
  v.closure = remote_ptr(clos);
  return v;
}

static inline void returnn(Stack stack) {
  PRINT_DEBUG_INFO;
  Value v = pop_stack(stack);
  Value c = pop_stack(stack);
  int my_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  if(c.closure.location == my_rank) {
    push_stack(stack, v);
    Closure lc = * (Closure*) c.closure.ptr;
    lc.code(lc.env, stack);
  } else {
    send_return(c.closure, v);
  }
}

static inline void apply(Env env, Stack stack, Code cont) {
  PRINT_DEBUG_INFO;
  Value c = pop_stack(stack);
  Value v = pop_stack(stack);
  if(c.closure.location == LOCATION_NONE) {
    ERROR("Applying something that is not a closure\n");
  }
  push_stack(stack, closure(cont, env));
  int my_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  if(c.closure.location == my_rank) {
    Closure lc = * (Closure*) c.closure.ptr;
    Env lcEnv = copy_env(lc.env);
    push_env(lcEnv, v);
    lc.code(lcEnv, stack);
  } else {
    send_apply(c.closure, v, stack);
  }
}

static inline void cond(Env env, Stack stack, Code t, Code f) {
  PRINT_DEBUG_INFO;
  Value v = pop_stack(stack);
  if(v.integer) {
    t(env, stack);
  } else {
    f(env, stack);
  }
}

#endif
