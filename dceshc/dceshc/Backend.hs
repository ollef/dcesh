module Backend(compile) where

import Control.Applicative
import Control.Monad.Trans.RWS
import Data.List
import Data.Monoid
import Data.Text(Text)
import qualified Data.Text as T

import AST
import Auxiliary
import ByteCode hiding (compile)

type CName = Int

cName :: CName -> Text
cName = s . ('f' :) . show

data CompiledFun = CompiledFun
  { cfName    :: CName
  , cfNode    :: Node
  , cfVisible :: Bool
  , cfCode    :: CodeF CName
  } deriving (Show, Eq)

type Linker a = RWS Node [CompiledFun] [CName] a

runLinker :: Linker a -> Node -> (a, [CompiledFun])
runLinker l n = evalRWS l n [0..]

newCompiledFun :: Bool -> Code -> Linker CName
newCompiledFun visible c = do
  lc <- link c
  n:ns <- get
  put ns
  node <- ask
  tell [CompiledFun
    { cfName = n
    , cfNode = node
    , cfVisible = visible
    , cfCode = lc
    }]
  return n

link :: Code -> Linker (CodeF CName)
link (In code) = case code of
  i :. c -> (:.) <$> linkInstr i <*> link (In c)
  Apply cont -> Apply <$> newCompiledFun False cont
  Remote n c cont -> Remote n <$> local (const n) (newCompiledFun True c)
                              <*> newCompiledFun False cont
  Cond c1 c2 -> Cond <$> newCompiledFun False c1 <*> newCompiledFun False c2
  Return -> return Return
  Done   -> return Done
  where
    linkInstr :: InstrF Code -> Linker (InstrF CName)
    linkInstr instr = case instr of
      Access v       -> return $ Access v
      Closure vs n c -> Closure vs n <$> newCompiledFun False c
      BeginLet       -> return BeginLet
      EndLet         -> return EndLet
      PushLit l      -> return $ PushLit l
      Prim p         -> return $ Prim p

s :: String -> Text
s = T.pack
sshow :: Show a => a -> Text
sshow = s . show

indent :: [Text] -> [Text]
indent = map (s"\t" <>)

scope :: Text -> [Text] -> [Text]
scope f b | T.null f = mconcat
  [ [s"{"]
  , indent b
  , [s"}"]
  ]
scope f b | otherwise = mconcat
  [ [f `space` s"{"]
  , indent b
  , [s"}"]
  ]

parens :: [Text] -> Text
parens xs = s"("
  <> mconcat (intersperse (s", ") xs)
  <> s")"

space :: Text -> Text -> Text
space a b = a <> s" " <> b

decl :: String -> Text -> Text
decl t x   = s t `space` x

dStack, dEnv, dVoid :: Text -> Text
dStack = decl "Stack"
dEnv   = decl "Env"
dVoid  = decl "void"

env, stack :: Text
env    = s"env"
stack  = s"stack"

rankOf :: Node -> Text
rankOf n = s"RANK_" <> s n

compileCode :: CodeF CName -> [Text]
compileCode code = case code of
  i :. c -> compileInstr i ++ compileCode c
  Apply cont -> [apply [env, stack, cName cont]]
  Remote n c cont ->
    [ pushStack [stack, closure [cName cont, env]]
    , sendRemote [rankOf n, sshow c, stack]
    ]
  Return -> [returnn [stack]]
  Cond c1 c2 -> [cond [env, stack, cName c1, cName c2]]
  Done   -> []
  where
    compileInstr :: InstrF CName -> [Text]
    compileInstr instr = case instr of
      Access v -> [pushStack [stack, deref $ accessEnv [env, sshow v]]]
      Closure vs n c -> mempty `scope`
                        (dEnv tmpEnv =: mallocEnv [sshow n]
                        : [pushEnv [tmpEnv, deref $ accessEnv [env, sshow v]] | v <- vs]
                        ++ [pushStack [stack, closure [cName c, tmpEnv]]]
                        )
      BeginLet     -> [pushEnv [env, popStack' [stack]]]
      EndLet       -> [popEnv [env]]
      PushLit (LitInt n) -> [pushStack [stack, intVal [sshow n]]]
      Prim p       -> mempty `scope`
                        [ dValue tmp1 =: popStack [stack]
                        , dValue tmp2 =: popStack [stack]
                        , pushStack [stack, intVal [fInt tmp1 <> prim p <> fInt tmp2]]
                        ]
        where tmp1 = s"tmp_1"
              tmp2 = s"tmp_2"

    deref x    = s"*" <> x
    tmpEnv     = s"tmp_env"
    dValue     = decl "Value"
    returnn    = scall "returnn"
    mallocEnv  = scall "malloc_env"
    pushEnv    = scall "push_env"
    popEnv     = scall "pop_env"
    accessEnv  = call "access_env"
    closure    = call "closure"
    intVal     = call "integer_value"
    popStack   = scall "pop_stack"
    popStack'  = call "pop_stack"
    pushStack  = scall "push_stack"
    sendRemote = scall "send_remote"
    apply      = scall "apply"
    cond       = scall "cond"
    fInt       = flip field "integer"
    prim Add   = s" + "
    prim Mul   = s" * "
    prim Sub   = s" - "
    prim Div   = s" / "
    prim Eq   = s" == "
    prim NEq   = s" != "
    prim Gr   = s" > "
    prim Le   = s" < "
    prim GEq   = s" >= "
    prim LEq   = s" <= "
    a =: b     = a <> s" = " <> b
    scall a bs = call a bs <> s";"
    call a bs  = s a <> parens bs
    field x f  = x <> s"." <> s f

compile :: Node -> Text -> Code -> [(Node, Text)]
compile startNode root code = mkRoot : cc
  where
    (startCode, cfs) = runLinker (newCompiledFun True code) startNode
    cfs' = [(cfNode x, xs) | xs@(x:_) <- sortedGroupBy cfNode cfs]
    ncfs' = zip [(1 :: Int)..] cfs'
    cc = [(node, mkNode funs) | (node, funs) <- cfs']
    mkNode funs = T.unlines
                $ [header]
               ++ [mkFunDecl (cfName cf) <> s";" | cf <- funs]
               ++ mconcat [mkFun (cfName cf) (compileCode $ cfCode cf) | cf <- funs]
               ++ mkReceiveRemote funs
               ++ mkMain
    mkFunDecl name = dVoid (cName name) <> parens [dEnv env, dStack stack]
    mkFun name c = mkFunDecl name `scope` (s"PRINT_DEBUG_INFO;" : c)
    header = T.unlines
      $ s"#include \"prelude.h\""
      : [s"#define" `space` rankOf node `space` sshow num
        | (num, (node, _)) <- ncfs']
    mkReceiveRemote funs =
      s"void receive_remote(int code, Env env, Stack stack)" `scope`
        (s"switch(code)" `scope`
          ([s"case" `space` sshow (cfName fun) <> s":" `space` cName (cfName fun) <> s"(env, stack); break;"
           | fun <- funs, cfVisible fun]
          ++ [s"default: break;"]))
    mkMain = s"int main(int argc, char** argv)" `scope`
      [ s"initialise(argc, argv);"
      , s"main_loop();"
      ]
    startNodeRank = head [rank | (rank, (node, _)) <- ncfs', node == startNode]
    mkRoot = ("root", T.unlines
      [ s"#define STARTNODE_RANK" `space` sshow startNodeRank
      , s"#define STARTNODE_CODE" `space` sshow startCode
      , root
      ])
