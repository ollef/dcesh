module ByteCode where

import Data.List
import Data.Maybe
import qualified Data.Set as S

import AST
import Auxiliary

type CVar = Int

data InstrF c
  = Access CVar
  | Closure [CVar] Int c
  | BeginLet | EndLet
  | PushLit Lit
  | Prim PrimOp
  deriving (Eq, Show)

instance Eq2   InstrF where eq2 = (==)
instance Show2 InstrF where show2 = show

data CodeF c
  = InstrF c :. CodeF c
  | Remote Node c c
  | Apply c
  | Cond c c
  | Return
  | Done
  deriving (Eq, Show)

instance Eq2   CodeF where eq2 = (==)
instance Show2 CodeF where show2 = show

type Code = Fix CodeF

compile :: Term -> Code
compile = flip (compile' []) $ In Return -- This is for returning to the root
  where
    compile' :: [Var] -> Term -> Code -> Code
    compile' vs term k = case term of
      Var v      -> Access (el vs v) |> k
      Abs v t    -> let vs' = S.toList $ fv term in
        Closure (map (el vs) vs') (length vs' + extraEnvSize t + 2)
                (compile' (vs' ++ [v]) t $ In Return) |> k
      App t t'   -> compile' vs t' $ compile' vs t $ In $ Apply k
      AtNode t n | S.null fvs -> In $ Remote n (compile' [] t $ In Return) k
                 | otherwise  -> compile' vs
                                          (foldl App (foldr Abs t lfvs `AtNode` n)
                                                     (map Var lfvs))
                                          k
        where
          fvs = fv t
          lfvs = S.toList fvs
      Let v t t' -> let vs' = vs ++ [v] in
        compile' vs t $ BeginLet |> compile' vs' t' (EndLet |> k)
      Lit l      -> PushLit l |> k
      PrimOp t p t' -> compile' vs t' $ compile' vs t $ Prim p |> k
      If t1 t2 t3 -> compile' vs t1 $ In $ Cond (compile' vs t2 k) (compile' vs t3 k)
      where
        (|>) :: InstrF Code -> Code -> Code
        i |> In c = In $ i :. c

        el :: (Eq a, Show a) => [a] -> a -> Int
        el xs x = fromMaybe (error $ "Not in scope: " ++ show x) $ elemIndex x xs
