{-# LANGUAGE FlexibleContexts #-}
module Auxiliary where

import Data.Function
import Data.List
import Data.Ord

-------------------------------------------------------------------------------
-- Type-level fix points
-------------------------------------------------------------------------------
newtype Fix f = In {out :: f (Fix f)}

cata :: Functor f => (f a -> a) -> Fix f -> a
cata f = f . fmap (cata f) . out

class Eq2 f where
  eq2 :: Eq a => f a -> f a -> Bool
class Eq2 f => Ord2 f where
  compare2 :: Ord a => f a -> f a -> Ordering
class Show2 f where
  show2 :: Show a => f a -> String

instance Eq2 f => Eq (Fix f) where
  In f == In g = eq2 f g
instance Ord2 f => Ord (Fix f) where
  compare (In f) (In g) = compare2 f g
instance Show2 f => Show (Fix f) where
  show (In f) = show2 f
-------------------------------------------------------------------------------
sortedGroupBy :: Ord b => (a -> b) -> [a] -> [[a]]
sortedGroupBy f = groupBy ((==) `on` f) . sortBy (comparing f)
