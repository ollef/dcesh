module AST where

import Data.Monoid
import Data.Set(Set)
import qualified Data.Set as S

type Var  = String
type Node = String

data Lit = LitInt Int
  deriving (Eq, Show)
data PrimOp = Add | Mul | Sub | Div | Eq | NEq | Gr | Le | GEq | LEq
  deriving (Eq, Show)

data Term
  = Var Var
  | Abs Var Term
  | App Term Term
  | AtNode Term Node
  | Let Var Term Term
  | Lit Lit
  | PrimOp Term PrimOp Term
  | If Term Term Term
  deriving (Eq, Show)

infixl 4 `App`

class FreeVars a where
  fv :: a -> Set Var

extraEnvSize :: Term -> Int
extraEnvSize term = case term of
  Var _      -> 0
  Abs _ _    -> 0
  App t t'   -> max (extraEnvSize t) (extraEnvSize t')
  AtNode _ _ -> 0
  Let _ t t' -> max (extraEnvSize t) (extraEnvSize t')
  Lit _      -> 0
  PrimOp t _ t' -> max (extraEnvSize t) (extraEnvSize t')
  If t1 t2 t3  -> maximum $ map extraEnvSize [t1, t2, t3]

instance FreeVars Term where
  fv term = case term of
    Var v      -> S.singleton v
    Abs v t    -> fv t S.\\ S.singleton v
    App t t'   -> fv t `mappend` fv t'
    AtNode t _ -> fv t
    Let v t t' -> fv t `mappend` (fv t' S.\\ S.singleton v)
    Lit _      -> mempty
    PrimOp t _ t' -> fv t `mappend` fv t'
    If t1 t2 t3 -> mconcat $ map fv [t1, t2, t3]
